package com.annutils.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.NoSuchElementException;

import static com.annutils.utils.TypeHelper.newInstance;


/**
 * Created by andreyzakharov on 13.08.14.
 */
public class JSONParser<T extends JsonModel> {


    private final Class<T> cls;

    public JSONParser(Class<T> cls){
        this.cls = cls;
    }


    @Nullable
    public T parse(@NonNull JSONObject jsonObject) throws JSONException, IllegalAccessException {

        T obj = newInstance(cls);

        for(Field field : obj.getClass().getDeclaredFields()){
           if(field.isAnnotationPresent(Json.class)){
               Annotation annotation = field.getAnnotation(Json.class);
               Json json = (Json) annotation;
               readAndSetFieldValue(field, obj, jsonObject, json);
           }
        }
        return obj;
    }

    private void readAndSetFieldValue(Field field, JsonModel obj, JSONObject jsonObject, Json jsonAnn){

        Class<?> fieldType = field.getType();
        Object val = null;
        try {
            val = readFromJson(jsonObject, jsonAnn, field);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if(val != null) {
            try {
               setFieldVal(field,obj,fieldType,val);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private void setFieldVal(Field field, JsonModel obj, Class<?> fieldType,  Object val) throws IllegalAccessException {
        if (TypeHelper.isInteger(fieldType)) {
            field.setInt(obj, (Integer) val);
        } else if (TypeHelper.isString(fieldType)) {
            field.set(obj, val);
        } else if(TypeHelper.isBoolean(fieldType)){
            field.setBoolean(obj, (Boolean) val);
        } else if(TypeHelper.isDouble(fieldType)){
            field.setDouble(obj, (Double) val);
        } else if(TypeHelper.isFloat(fieldType)){
            field.setFloat(obj, (Float) val);
        } else if(TypeHelper.isLong(fieldType)){
            field.setLong(obj, (Long) val);
        } else if(TypeHelper.isModel(fieldType)){
            field.set(obj,val);
        } else if(TypeHelper.isArray(fieldType)){
            field.set(obj,val);
        } else if(TypeHelper.isCollection(fieldType)){
            field.set(obj,val);
        }
    }


    @Nullable
    private Object readFromJson(@NonNull JSONObject jsonObject, Json jsonAnn, Field field) throws JSONException, IllegalAccessException {
        boolean isOptional = jsonAnn.optional();
        String key = jsonAnn.name();

        if(jsonObject.has(key)){
            JsonReader jsonReader = new JsonReader();
            return jsonReader.read(jsonObject,field,key);
        }
        else if(!isOptional){
            throw new JSONException(String.format("Key %s is not optional, but not found in JSON object", key));
        }

        return null;
    }



}
