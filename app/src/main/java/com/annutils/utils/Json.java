package com.annutils.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by andreyzakharov on 14.08.14.
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Json {

    String name() default "";

    boolean optional() default false;
}
