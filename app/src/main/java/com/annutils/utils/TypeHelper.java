package com.annutils.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Created by andreyzakharov on 14.08.14.
 */
public class TypeHelper {


    public static boolean isInteger(Class<?> fieldType){
        return fieldType == int.class;
    }

    public static boolean isLong(Class<?> fieldType) { return fieldType == long.class; }

    public static boolean isString(Class<?> fieldType){
        return fieldType == String.class;
    }

    public static boolean isBoolean(Class<?> fieldType) {
        return fieldType == boolean.class;
    }

    public static boolean isDouble(Class<?> fieldType) { return fieldType == double.class; }

    public static boolean isFloat(Class<?> fieldType) { return fieldType==float.class; }

    public static boolean isCollection(Class<?> fieldType) { return Collection.class.isAssignableFrom(fieldType);}

    public static boolean isGenericObject(Class<?> fieldType) { return GenericDeclaration.class.isAssignableFrom(fieldType); }

    public static boolean isModel(Class<?> fieldType) { return JsonModel.class.isAssignableFrom(fieldType); }

    public static boolean isArray(Class<?> cls) { return cls.isArray(); }

    public static boolean isJSONObject(Class<?> cls) {
        return JSONObject.class.isAssignableFrom(cls);
    }

    public static boolean isJSONArray(Class<?> cls) {
        return JSONArray.class.isAssignableFrom(cls);
    }


    public static Class<?>[] getFieldGenericArgs(Field field) {
        Type genericType = field.getGenericType();
        if (genericType instanceof ParameterizedType) {
            Type[] typeArr = ((ParameterizedType) genericType)
                    .getActualTypeArguments();
            Class<?>[] argsArr = new Class<?>[typeArr.length];
            for (int i = 0; i < typeArr.length; i++) {
                String[] nameParts = typeArr[i].toString().split(" ");
                String clsName = nameParts[nameParts.length - 1];
                argsArr[i] = classForName(clsName);
            }
            return argsArr;
        } else {
            return new Class<?>[0];
        }
    }

    public static Class<?> classForName(String clsName)
            throws IllegalArgumentException {
        try {
            return Class.forName(clsName);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static <T> T newInstance(Class<T> cls) throws IllegalArgumentException {
        try {
            return cls.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
