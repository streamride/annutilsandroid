package com.annutils.utils;

import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import static com.annutils.utils.TypeHelper.newInstance;

/**
 * Created by andreyzakharov on 18.08.14.
 */
public class JsonReader {


    public Object read(JSONObject jsonObject, Field field, String key) throws JSONException, IllegalAccessException {
        Class<JsonModel> fieldType = (Class<JsonModel>) field.getType();
        if(TypeHelper.isInteger(fieldType))
            return jsonObject.getInt(key);
        else if(TypeHelper.isString(fieldType))
            return jsonObject.getString(key);
        else if(TypeHelper.isLong(fieldType))
            return jsonObject.getLong(key);
        else if(TypeHelper.isCollection(fieldType)){
            Class<?> componentType = TypeHelper.getFieldGenericArgs(field)[0];
            String string = jsonObject.getString(key);
            return readJsonCollection(fieldType, componentType, string);
        }
        else if(TypeHelper.isFloat(fieldType)){
            String floatValStr = jsonObject.getString(key);
            return Float.valueOf(floatValStr);
        }
        else if(TypeHelper.isDouble(fieldType)){
            return jsonObject.getDouble(key);
        }
        else if(TypeHelper.isArray(fieldType)){
            return readJsonArray(fieldType, fieldType.getComponentType(), jsonObject.getString(key));

        }
        else if(TypeHelper.isModel(fieldType)){
            JSONObject jsonObject1 = jsonObject.getJSONObject(key);
            JSONParser<JsonModel> parser = new JSONParser<JsonModel>(fieldType);
            JsonModel jsonModel = parser.parse(jsonObject1);
            return jsonModel;
        }
        return null;
    }



    private Object readJsonCollection(Class<?> fieldType, Class<?> componentType, @NonNull String json) throws JSONException, IllegalAccessException {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(json);
        } catch (JSONException e) {
            throw new IllegalArgumentException(e);
        }

        boolean isModel = TypeHelper.isModel(componentType);
        JSONParser<JsonModel> jsonParser = null;
        if(isModel){
            jsonParser = new JSONParser<JsonModel>((Class<JsonModel>) componentType);
        }


        Collection<Object> coll = (Collection<Object>) newInstance(fieldType);

        if(jsonArray != null && jsonArray.length() > 0) {
            for(int i=0;i<jsonArray.length();i++){
                 Object obj = jsonArray.get(i);
                if(isModel){
                    obj = jsonParser.parse((JSONObject) obj);
                }
                coll.add(obj);
            }
        }
        return coll;

    }

    private Object readJsonArray(Class<JsonModel> fieldType, Class<?> componentType, @NonNull String json) throws JSONException, IllegalAccessException {

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(json);
        } catch (JSONException e) {
            throw new IllegalArgumentException(e);
        }

        boolean isModel = TypeHelper.isModel(componentType);



        if(jsonArray != null && jsonArray.length() > 0){
            Object[] arr = new Object[jsonArray.length()];

            JSONParser<JsonModel> jsonParser = new JSONParser<JsonModel>((Class<JsonModel>) componentType);
            for(int i=0;i<jsonArray.length();i++){
                Object obj = jsonArray.get(i);
                if(isModel){
                    obj = jsonParser.parse((JSONObject) obj);
                }
                arr[i] = obj;
            }

            if(isModel){
                Object modelArr = Array.newInstance(componentType, arr.length);
                for (int i = 0; i < arr.length; i++) {
                    Array.set(modelArr, i, arr[i]);
                }
                return modelArr;
            }
            else {
                String[] arr2 = new String[arr.length];
                for (int i = 0; i < arr.length; i++) {
                    arr2[i] = arr[i].toString();
                }

                Object parseTypeArr = parseTypeArr(componentType, arr2);
                return parseTypeArr;
            }


        }
        return null;
    }

    private final <T> Object parseTypeArr(Class<T> valType, String[] arr) {
        Object objArr = Array.newInstance(valType, arr.length);
        for (int i = 0; i < arr.length; i++) {
            T item = null;
            if(TypeHelper.isInteger(valType)){
                item = (T) Integer.valueOf(arr[i]);
            }
            else if(TypeHelper.isLong(valType)){
                item = (T) Long.valueOf(arr[i]);
            }
            else if(TypeHelper.isDouble(valType)){
                item = (T) Double.valueOf(arr[i]);
            }
            else if(TypeHelper.isFloat(valType)){
                item = (T) Float.valueOf(arr[i]);
            }

            if(item != null)
                Array.set(objArr, i, item);
        }
        return objArr;
    }




}
