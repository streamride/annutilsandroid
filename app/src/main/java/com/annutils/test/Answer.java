package com.annutils.test;

import android.text.TextUtils;

import com.annutils.utils.Json;
import com.annutils.utils.JsonModel;

/**
 * Created by andreyzakharov on 15.08.14.
 */
public class Answer extends JsonModel{


    @Json(name = "user_name", optional = false)
    public String userName;

    @Json(name = "text")
    public String text;


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if(!TextUtils.isEmpty(userName))
            stringBuilder.append(userName);
        if(!TextUtils.isEmpty(text))
            stringBuilder.append(text);
        return stringBuilder.toString();
    }
}
