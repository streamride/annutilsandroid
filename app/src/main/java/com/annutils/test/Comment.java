package com.annutils.test;

import android.text.TextUtils;

import com.annutils.utils.Json;
import com.annutils.utils.JsonModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 15.08.14.
 */
public class Comment extends JsonModel{

    @Json(name = "text")
    public String text;

    @Json(name = "user_name")
    public String userName;

//    @Json(name = "answer")
//    public Answer answer;

    @Json(name = "dt")
    public long date;

    @Json(name = "answers")
    public Answer[] answerList;

    @Json(name = "permissions")
    public ArrayList<Integer> permissions;

    @Json(name = "lat")
    public float lat;


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if(!TextUtils.isEmpty(text))
            builder.append(text).append(" ");
        if(!TextUtils.isEmpty(userName))
            builder.append(userName).append(" ");
//        if(answer != null)
//            builder.append(answer.toString());
        if(date != 0)
            builder.append(date).append(" ");
        if(permissions != null){
            for(int i=0;i<permissions.size();i++)
                builder.append(permissions.get(i)).append(" ");
        }
        if(answerList != null){
            for (Answer answer1 : answerList)
                builder.append(answer1.toString()).append(" ");
        }
        builder.append(lat).append(" ");
        return builder.toString();
    }
}
